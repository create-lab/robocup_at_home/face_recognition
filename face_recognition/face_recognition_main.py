import os
import cv2
from .functions.face_recog import face_recognition
from .functions.visualization_fr import visualize_face_recognition
from perception_interfaces.srv import GetFaceRecognition
from geometry_msgs.msg import TransformStamped

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge


import copy

# TODO: add z (depth)
# TODO: add name back to database
# TODO: add world frame position back to database

class FaceRecogNode(Node):
    def __init__(self):
        super().__init__('face_recognition')
        self.representations=[]

        self.subscription_image = self.create_subscription(
            Image,
            '/zedm/zed_node/left_raw/image_raw_color', 
            self.image_callback,
            10)
        
        self.srv = self.create_service(GetFaceRecognition, 'get_face_recognition', self.get_face_recognition_callback)
        self.cv_bridge = CvBridge()
        self.last_time = self.get_clock().now()


    def image_callback(self, msg):
        #self.get_logger().info("Received an image!")
        self.image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
    
    def box_to_middle_point(self,box):
        return ((box.x+box.w/2),(box.y+box.h/2))
        
# TODO: Test: time stamp and get_face_pose
    
    def get_face_recognition_callback(self, request, response):
        self.get_logger().info("Received a request! Start face recognition.")

        self.current_time = self.get_clock().now()
        # Calculate the time difference in seconds
        time_difference = (self.current_time - self.last_time).nanoseconds / 1e9
        # if time_difference larger than 1 second, update the face database
        if time_difference >= 1.0:
            self.update=True
            self.last_time = copy.copy(self.current_time)
        else:
            self.update=False

        self.face_infos, self.face_bbxs=face_recognition(self.image,self.current_time,self.update)
        response.face_info=self.face_infos
        response.face_bbx=self.face_bbxs
        face_points=[self.box_to_middle_point(x) for x in self.face_bbxs]
        response.face_transform=[self.get_face_pose(x,info) for x,info in zip(face_points,self.face_infos)]
        response.header.stamp=self.current_time

        self.get_logger().info("Finished face recognition.")
        print(request.visualization)
        if request.visualization:
            image=visualize_face_recognition(self.image, self.face_bbxs, self.face_infos)
            cv2.imshow('Image with Detected Face', image)
            cv2.waitKey(1)
        
        return response
    
    def get_face_pose(self, point, info):

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = "camera"
        t.child_frame_id = info.id

        # no rotation for the object (it is just seen as a point)
        t.transform.rotation.x = 0.0
        t.transform.rotation.y = 0.0
        t.transform.rotation.z = 0.0
        t.transform.rotation.w = 1.0

        # object coordinates
        t.transform.translation.x = point[0]
        t.transform.translation.y = point[1]
        t.transform.translation.z = 0

        return t

        

def main(args=None):
    rclpy.init(args=args)

    face_recognition_node = FaceRecogNode()

    while rclpy.ok():
       
        rclpy.spin_once(face_recognition_node)

    face_recognition_node.destroy_node()
    rclpy.shutdown()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
