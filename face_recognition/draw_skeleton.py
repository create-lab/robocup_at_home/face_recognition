import cv2
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from zed_interfaces.msg import Skeleton2D, Object, ObjectsStamped
import math
import time

# # Global variable to store skeleton data
# skeleton_data = None
# image_data = None

class SkeletonDrawerNode(Node):
    def __init__(self):
        super().__init__('skeleton_drawer')
        self.subscription_skeleton = self.create_subscription(
            ObjectsStamped,
            '/zedm/zed_node/body_trk/skeletons',  # Replace with the actual topic
            self.skeleton_callback,
            10)
        self.subscription_image = self.create_subscription(
            Image,
            '/zedm/zed_node/left_raw/image_raw_color',  # Replace with the actual topic
            self.image_callback,
            10)
        self.cv_bridge = CvBridge()
        self.skeleton_data=None
        self.image_data=None
        self.image_time_stamp=None
        self.skeleton_time_stamp=None
        self.last_skeleton_time_stamp=None
        self.draw_finish=True

    def convert_to_nanosec(self, time_stamp):
        return time_stamp.sec*1000000000+time_stamp.nanosec

    def skeleton_callback(self, msg):
        # print('Get skeleton data')
        
        skeleton_data=[]
        objects=msg.objects
        for object in objects:
            skeleton_data_per_object=[]
            kps=object.skeleton_2d.keypoints
            for kp_one in kps[:38]:
                skeleton_data_per_object.append(kp_one.kp)
            skeleton_data.append(skeleton_data_per_object)

        self.skeleton_data=skeleton_data
    
    def image_callback(self, msg):
        # print('Get image data')

        self.image_time_stamp=msg.header.stamp
        image_data = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        width = int(image_data.shape[1] * 2)
        height = int(image_data.shape[0] * 2)
        dim = (width, height)
        
        # resize image
        resized = cv2.resize(image_data, dim, interpolation = cv2.INTER_AREA)

        self.image_data=resized

    def get_skeleton_data(self):
        return self.skeleton_data
    def get_image_data(self):
        return self.image_data
    def set_skeleton_data(self):
        self.skeleton_data=None
    def set_image_data(self):
        self.image_data=None
   

def draw_skeleton_on_image(image, keypoints):
    # # Define connections in the skeleton
    # connections = [
    #     (0, 1), (1, 2), (2, 3),  # Head
    #     (4, 5), (5, 6), (6, 7),  # Body
    #     (8, 9), (9, 10),         # Left Arm
    #     (8, 11), (11, 12),       # Right Arm
    #     (0, 4), (0, 8)           # Body connections
    # ]

    # Define connections in the skeleton
    # The BODY_38 body format contains 38 keypoints 
    # define in https://www.stereolabs.com/docs/body-tracking/#tabBody38
    connections = [
        (9, 7), (8, 6), (6, 5),(7,5),(5,4),  # Head
        (4,3),(3,2),(2,1),(1,0),  # Body
        (31, 17), (33,17),(35,17),(37,17),(17,15),(15,13),(13,11),(11,3), # Right Arm
        (30,16),(32,16),(34,16),(36,16),(16,14),(14,12),(12,10),(10,3), # Left Arm
        (25,23),(27,23),(29,23),(23,21),(21,19),(19,0),  # Right Leg
        (24,22),(26,22),(28,22),(22,20),(20,18),(18,0)   # Left Leg
    ]

    # Draw keypoints
    for keypoint_object in keypoints:
        for point in keypoint_object:
            x, y = int(point[0]), int(point[1])
            cv2.circle(image, (x, y), 5, (0, 255, 0), -1)

    # Draw connections
    for connection in connections:
        for keypoint_object in keypoints:
            start_point = (int(keypoint_object[connection[0]][0]), int(keypoint_object[connection[0]][1]))
            end_point = (int(keypoint_object[connection[1]][0]), int(keypoint_object[connection[1]][1]))
            if any(x < 0 for x in start_point): continue
            if any(x < 0 for x in end_point): continue
            cv2.line(image, start_point, end_point, (0, 255, 0), 2)

def main(args=None):
    rclpy.init(args=args)

    skeleton_drawer_node = SkeletonDrawerNode()

    while rclpy.ok():
        # if image_data is not None:
        image_data=skeleton_drawer_node.get_image_data()
        skeleton_data=skeleton_drawer_node.get_skeleton_data()
        
        if skeleton_data is not None and image_data is not None:
            skeleton_drawer_node.set_image_data()
            skeleton_drawer_node.set_skeleton_data()

            # Draw the skeleton on the image
            draw_skeleton_on_image(image_data, skeleton_data)

            # Display the image with the skeleton
            cv2.imshow('Skeleton Drawer', image_data)

        # Break the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        rclpy.spin_once(skeleton_drawer_node)

    cv2.destroyAllWindows()

    skeleton_drawer_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
