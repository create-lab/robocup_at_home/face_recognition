from .deepface_func import deepface_find
from deepface import DeepFace
import os
import cv2
import pandas as pd
import pickle
import json
from perception_interfaces.msg import FaceInfo, FaceBox



import rclpy
from rclpy.node import Node


from ament_index_python.packages import get_package_share_directory

# print(os.path.dirname(__file__))
# exit()

package_share_directory = get_package_share_directory('face_recognition')
# Find the index of the last occurrence of 'install' in the path
index_of_install = package_share_directory.rfind('install')
# Extract everything before 'install'
ws_directory = os.path.abspath(package_share_directory[:index_of_install])
data_directory=os.path.join(ws_directory,'src/face_recognition/data')

DB_PATH=os.path.join(data_directory,"face_db")
FACE_DIC_PATH=os.path.join(data_directory,"face_dictionary.pkl")

MODEL_NAME= 'Facenet512'
DISTANCE_METRIC= 'euclidean_l2'

REPRESENTATION_FILE = f"representations_{MODEL_NAME}.pkl"
REPRESENTATION_FILE = REPRESENTATION_FILE.replace("-", "_").lower()

REPRESENTATION_PATH = os.path.join(DB_PATH,REPRESENTATION_FILE)


backends = [
  'opencv', 
  'ssd', 
  'dlib', 
  'mtcnn', 
  'retinaface', 
  'mediapipe',
  'yolov8',
  'yunet',
  'fastmtcnn',
]

def face_recognition(image,detect_time,update):


    face_infos=[]
    representations=[]
    face_bbxs=[]

    new_face_found=0
    old_face_found=0
    
    with open(FACE_DIC_PATH, 'rb') as f:
        ppl_dic=pickle.load(f)
    # print('Current face dictionary:',ppl_dic)

    # face recognition
    dfs = deepface_find(img_path = image, db_path = DB_PATH, enforce_detection=False,model_name=MODEL_NAME,distance_metric=DISTANCE_METRIC,silent=True)
    # print(dfs)

    for df in dfs:
        new_face=False
        #Initialize face info message
        face_info=FaceInfo()
        face_box=FaceBox()

        x, y, w, h = df['source_x'].values[0], df['source_y'].values[0], df['source_w'].values[0], df['source_h'].values[0]
        face_box.x=int(x)
        face_box.y=int(y)
        face_box.w=int(w)
        face_box.h=int(h)
        # Crop the region from the image
        cropped_region = image[y:y+h, x:x+w]
        # face=cropped_region.copy()
        face=cv2.resize(cropped_region, (224, 224), interpolation = cv2.INTER_AREA)
        # print('df:',df)
        # print(df['identity'].values)
        if df['identity'].values[0]!=None:
            old_face_found+=1
            print("Known Face found")
            # get all the found faces id
            found_faces=[os.path.basename(path) for path in df['identity'].values]
            found_faces_id=[int(path.split('_')[0]) for path in found_faces]
            # get the most found face id
            found_faces_id= max(set(found_faces_id), key=found_faces_id.count)
            
            
            if update:
                print('Update known found faces')
                # get the all images names of most found face id
                found_faces_images=ppl_dic[found_faces_id]['images']
                # Get the number of this new face for this face id
                new_number=len(found_faces_images)
                new_image_path=str(found_faces_id)+"_"+str(new_number)+".jpg"
                objs = DeepFace.analyze(img_path = face, 
                    actions = ['emotion'],
                    enforce_detection = False,
                    detector_backend = 'skip',
                    silent=True,
                )
                # print(objs)
                objes=objs[0]
                new_emotion=objes['dominant_emotion']

                # Store new face to df
                ppl_dic[found_faces_id]['images'].append(new_image_path)
                # Store new face to df
                ppl_dic[found_faces_id]['emotions'].append(new_emotion)
            
                # save face to database with assign id name
                cv2.imwrite(os.path.join(DB_PATH,new_image_path),face)

            face_info.id=int(found_faces_id)
            face_info.status='old'
            face_info.image_names=ppl_dic[found_faces_id]['images']
            face_info.name=str(ppl_dic[found_faces_id]['name'])
            face_info.age=int(ppl_dic[found_faces_id]['age'])
            face_info.gender=ppl_dic[found_faces_id]['gender']
            face_info.race=str(ppl_dic[found_faces_id]['race'])
            face_info.emotions=ppl_dic[found_faces_id]['emotions']

        else:
            new_face=True
            new_face_found+=1
            print("New face found")
            print("Detecting other features")
            objs = DeepFace.analyze(img_path = face, 
                actions = ['age', 'gender', 'race', 'emotion'],
                enforce_detection = False,
                detector_backend = 'skip',
                silent=False,
            )
            # print(objs)
            objes=objs[0]
            new_age=objes['age']
            new_gender=objes['dominant_gender']
            new_race=objes['dominant_race']
            new_emotion=objes['dominant_emotion']
            # print(objs)
            print("Adding face to database")
            new_id=len(ppl_dic)-1
            # image name is face_id_#no.jpg
            new_image_path=str(new_id)+"_0.jpg"
            new_name=None
            # Add new face to dictionary
            ppl_dic[new_id]={'id': new_id, 'images':[new_image_path],'name':new_name, 'age':new_age ,'gender':new_gender,'race':new_race,'emotions':[new_emotion]}
            
            # save face to database with assign id name
            cv2.imwrite(os.path.join(DB_PATH,new_image_path),face)
            print('Successfully added new face to database')

            face_info.id=int(new_id)
            face_info.status='new'
            face_info.image_names=ppl_dic[new_id]['images']
            face_info.name=str(ppl_dic[new_id]['name'])
            face_info.age=int(ppl_dic[new_id]['age'])
            face_info.gender=ppl_dic[new_id]['gender']
            face_info.race=str(ppl_dic[new_id]['race'])
            face_info.emotions=ppl_dic[new_id]['emotions']

        face_infos.append(face_info)
        face_bbxs.append(face_box)
        if update or new_face:
            # The first time do DeepFace.find, it will create representations_vgg_face.pkl. 
            # Add face (which add to db means every face detected) to the pkl rather than do embedding for whole db again.
            face_encoding = DeepFace.represent(img_path = face,enforce_detection = False, model_name = MODEL_NAME)
            representations.append([os.path.join(DB_PATH,new_image_path),face_encoding[0]["embedding"]])
            # representations needs to be the array with the new trained faces
   
    if dfs:
        with open(REPRESENTATION_PATH, 'rb') as f:
            representations += pickle.load(f)

        with open(REPRESENTATION_PATH, 'wb') as f:
            pickle.dump(representations, f)

        print(f'Found {new_face_found} new faces')
        print(f'Found {old_face_found} known faces')


        # Update face dictionary
        # print('Update face dictionary:',ppl_dic)
        #Save people dictionary to pkl
        with open(FACE_DIC_PATH, 'wb') as f:
            pickle.dump(ppl_dic, f)
        # print('Successfully update face dictionary to',FACE_DIC_PATH)

    return face_infos,face_bbxs
    