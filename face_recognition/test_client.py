import sys

from perception_interfaces.srv import GetFaceRecognition
from .functions.visualization_fr import visualize_face_recognition
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2


class MinimalClientAsync(Node):

    def __init__(self):
        super().__init__('minimal_client_async')
        self.cli = self.create_client(GetFaceRecognition, 'get_face_recognition')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = GetFaceRecognition.Request()
        self.response=None


    def send_request(self, visualization):
        self.req.visualization = visualization=='True'
        print('Sending request:',self.req.visualization)
        self.future = self.cli.call_async(self.req)
        rclpy.spin_until_future_complete(self, self.future)
        # print('Waiting for response')
        self.response=self.future.result()
        return self.response
    


def main(args=None):
    rclpy.init(args=args)

    minimal_client = MinimalClientAsync()

    while rclpy.ok():

        response = minimal_client.send_request(str(sys.argv[1]))
        minimal_client.get_logger().info(
            'Result:' + str(response.face_info)+str(response.face_bbx))

    minimal_client.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()